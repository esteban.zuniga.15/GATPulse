CREATE TABLE `subscribers` (
  `idsubscriber` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `initialdate` date NOT NULL,
  `bday` date NOT NULL,
  `email` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `emergencyphone` varchar(45) DEFAULT NULL,
  `photolink` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsubscriber`),
  UNIQUE KEY `phone_UNIQUE` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `routines` (
  `idroutine` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `datecreated` date NOT NULL,
  `idsubscriber` int(11) DEFAULT NULL,
  PRIMARY KEY (`idroutine`),
  KEY `idsubscriber_idx` (`idsubscriber`),
  CONSTRAINT `idsubscriber` FOREIGN KEY (`idsubscriber`) REFERENCES `subscribers` (`idsubscriber`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sets` (
  `idset` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `idroutine` int(11) NOT NULL,
  PRIMARY KEY (`idset`),
  KEY `idroutine_idx` (`idroutine`),
  CONSTRAINT `idroutine` FOREIGN KEY (`idroutine`) REFERENCES `routines` (`idroutine`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `exercisetype` (
  `idexerciseType` int(11) NOT NULL AUTO_INCREMENT,
  `typeName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idexerciseType`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE `exercises` (
  `idexercise` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `position` varchar(256) DEFAULT NULL,
  `instructions` varchar(256) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexercise`),
  KEY `type_idx` (`type`),
  CONSTRAINT `type` FOREIGN KEY (`type`) REFERENCES `exercisetype` (`idexerciseType`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `setexercise` (
  `idsetexc` int(11) NOT NULL AUTO_INCREMENT,
  `idset` int(11) NOT NULL,
  `idexercise` int(11) NOT NULL,
  `rep` int(11) NOT NULL,
  `pes` int(11) NOT NULL,
  `dur` int(11) NOT NULL,
  PRIMARY KEY (`idsetexc`),
  KEY `idset_idx` (`idset`),
  KEY `idexercise_idx` (`idexercise`),
  CONSTRAINT `idexercise` FOREIGN KEY (`idexercise`) REFERENCES `exercises` (`idexercise`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idset` FOREIGN KEY (`idset`) REFERENCES `sets` (`idset`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
