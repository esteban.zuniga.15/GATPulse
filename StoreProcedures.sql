DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `selectExercisesByRoutineId`(ridparam int(10))
BEGIN
select sets.id as 'set_id', exercises.name, exercises.instructions, exercises.position, exercises.type_id from sets
join sets_exercises on sets.id = sets_exercises.set_id
join exercises on sets_exercises.exercise_id = exercises.id
where sets.routine_id = ridparam
order by set_id;
END$$
DELIMITER ;
