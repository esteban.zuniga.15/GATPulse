<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Measurements Model
 *
 * @property \App\Model\Table\SubscribersTable|\Cake\ORM\Association\BelongsTo $Subscribers
 *
 * @method \App\Model\Entity\Measurement get($primaryKey, $options = [])
 * @method \App\Model\Entity\Measurement newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Measurement[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Measurement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Measurement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Measurement[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Measurement findOrCreate($search, callable $callback = null, $options = [])
 */
class MeasurementsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('measurements');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Subscribers', [
            'foreignKey' => 'subscriber_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('metabolicage')
            ->allowEmpty('metabolicage');

        $validator
            ->integer('bmr')
            ->allowEmpty('bmr');

        $validator
            ->integer('bonemass')
            ->allowEmpty('bonemass');

        $validator
            ->integer('height')
            ->allowEmpty('height');

        $validator
            ->integer('weight')
            ->allowEmpty('weight');

        $validator
            ->integer('fat')
            ->allowEmpty('fat');

        $validator
            ->integer('neck')
            ->allowEmpty('neck');

        $validator
            ->integer('rightarm')
            ->allowEmpty('rightarm');

        $validator
            ->integer('leftarm')
            ->allowEmpty('leftarm');

        $validator
            ->integer('wrist')
            ->allowEmpty('wrist');

        $validator
            ->integer('core')
            ->allowEmpty('core');

        $validator
            ->integer('hip')
            ->allowEmpty('hip');

        $validator
            ->integer('thorax')
            ->allowEmpty('thorax');

        $validator
            ->integer('righthigh')
            ->allowEmpty('righthigh');

        $validator
            ->integer('lefthigh')
            ->allowEmpty('lefthigh');

        $validator
            ->integer('rightcalve')
            ->allowEmpty('rightcalve');

        $validator
            ->integer('leftcalve')
            ->allowEmpty('leftcalve');

        $validator
            ->dateTime('creationdate')
            ->allowEmpty('creationdate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['subscriber_id'], 'Subscribers'));

        return $rules;
    }
}
