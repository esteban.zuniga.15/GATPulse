<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Subscribers Model
 *
 * @property \App\Model\Table\GymprofilesTable|\Cake\ORM\Association\BelongsTo $Gymprofiles
 * @property \App\Model\Table\InstructorsTable|\Cake\ORM\Association\BelongsTo $Instructors
 * @property \App\Model\Table\MeasurementsTable|\Cake\ORM\Association\HasMany $Measurements
 * @property \App\Model\Table\RoutinesTable|\Cake\ORM\Association\HasMany $Routines
 *
 * @method \App\Model\Entity\Subscriber get($primaryKey, $options = [])
 * @method \App\Model\Entity\Subscriber newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Subscriber[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Subscriber|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Subscriber patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Subscriber[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Subscriber findOrCreate($search, callable $callback = null, $options = [])
 */
class SubscribersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('subscribers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Gymprofiles', [
            'foreignKey' => 'gymprofile_id'
        ]);
        $this->belongsTo('Instructors', [
            'foreignKey' => 'instructor_id'
        ]);
        $this->hasMany('Measurements', [
            'foreignKey' => 'subscriber_id'
        ]);
        $this->hasMany('Routines', [
            'foreignKey' => 'subscriber_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('lastnames');

        $validator
            ->allowEmpty('birthday');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('phone');

        $validator
            ->allowEmpty('emergencyphone');

        $validator
            ->allowEmpty('photo');

        $validator
            ->allowEmpty('joined');

        $validator
            ->allowEmpty('sex');

        $validator
            ->requirePresence('idlegal', 'create')
            ->notEmpty('idlegal');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['gymprofile_id'], 'Gymprofiles'));
        $rules->add($rules->existsIn(['instructor_id'], 'Instructors'));

        return $rules;
    }
}
