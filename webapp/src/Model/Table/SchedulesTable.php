<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Schedules Model
 *
 * @property \App\Model\Table\GymprofilesTable|\Cake\ORM\Association\HasMany $Gymprofiles
 * @property \App\Model\Table\InstructorsTable|\Cake\ORM\Association\HasMany $Instructors
 * @property \App\Model\Table\TrainingsTable|\Cake\ORM\Association\HasMany $Trainings
 *
 * @method \App\Model\Entity\Schedule get($primaryKey, $options = [])
 * @method \App\Model\Entity\Schedule newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Schedule[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Schedule|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Schedule patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Schedule[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Schedule findOrCreate($search, callable $callback = null, $options = [])
 */
class SchedulesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('schedules');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Gymprofiles', [
            'foreignKey' => 'schedule_id'
        ]);
        $this->hasMany('Instructors', [
            'foreignKey' => 'schedule_id'
        ]);
        $this->hasMany('Trainings', [
            'foreignKey' => 'schedule_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('mon')
            ->allowEmpty('mon');

        $validator
            ->boolean('tue')
            ->allowEmpty('tue');

        $validator
            ->boolean('wed')
            ->allowEmpty('wed');

        $validator
            ->boolean('thu')
            ->allowEmpty('thu');

        $validator
            ->boolean('fri')
            ->allowEmpty('fri');

        $validator
            ->boolean('sat')
            ->allowEmpty('sat');

        $validator
            ->boolean('sun')
            ->allowEmpty('sun');

        $validator
            ->allowEmpty('starthour');

        $validator
            ->allowEmpty('endhour');

        return $validator;
    }
}
