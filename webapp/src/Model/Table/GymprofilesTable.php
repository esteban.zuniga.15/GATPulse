<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gymprofiles Model
 *
 * @property \App\Model\Table\SchedulesTable|\Cake\ORM\Association\BelongsTo $Schedules
 * @property \App\Model\Table\SubscribersTable|\Cake\ORM\Association\HasMany $Subscribers
 * @property \App\Model\Table\TrainingsTable|\Cake\ORM\Association\HasMany $Trainings
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Gymprofile get($primaryKey, $options = [])
 * @method \App\Model\Entity\Gymprofile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Gymprofile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Gymprofile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Gymprofile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Gymprofile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Gymprofile findOrCreate($search, callable $callback = null, $options = [])
 */
class GymprofilesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('gymprofiles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Schedules', [
            'foreignKey' => 'schedule_id'
        ]);
        $this->hasMany('Subscribers', [
            'foreignKey' => 'gymprofile_id'
        ]);
        $this->hasMany('Trainings', [
            'foreignKey' => 'gymprofile_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'gymprofile_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('url');

        $validator
            ->allowEmpty('address');

        $validator
            ->allowEmpty('state');

        $validator
            ->allowEmpty('country');

        $validator
            ->allowEmpty('postalcode');

        $validator
            ->allowEmpty('payment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['schedule_id'], 'Schedules'));

        return $rules;
    }
}
