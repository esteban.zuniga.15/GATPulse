<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sets Model
 *
 * @property \App\Model\Table\RoutinesTable|\Cake\ORM\Association\BelongsTo $Routines
 * @property \App\Model\Table\ExercisesTable|\Cake\ORM\Association\BelongsToMany $Exercises
 *
 * @method \App\Model\Entity\Set get($primaryKey, $options = [])
 * @method \App\Model\Entity\Set newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Set[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Set|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Set patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Set[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Set findOrCreate($search, callable $callback = null, $options = [])
 */
class SetsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sets');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Routines', [
            'foreignKey' => 'routine_id'
        ]);
        $this->belongsToMany('Exercises', [
            'foreignKey' => 'set_id',
            'targetForeignKey' => 'exercise_id',
            'joinTable' => 'sets_exercises'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('type')
            ->allowEmpty('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['routine_id'], 'Routines'));

        return $rules;
    }
}
