<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subscriber Entity
 *
 * @property int $id
 * @property string $name
 * @property string $lastnames
 * @property string $birthday
 * @property string $email
 * @property string $phone
 * @property string $emergencyphone
 * @property string $photo
 * @property int $gymprofile_id
 * @property string $joined
 * @property int $instructor_id
 * @property string $sex
 * @property string $idlegal
 *
 * @property \App\Model\Entity\Gymprofile $gymprofile
 * @property \App\Model\Entity\Instructor $instructor
 * @property \App\Model\Entity\Measurement[] $measurements
 * @property \App\Model\Entity\Routine[] $routines
 */
class Subscriber extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
