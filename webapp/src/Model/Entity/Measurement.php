<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Measurement Entity
 *
 * @property int $id
 * @property int $metabolicage
 * @property int $bmr
 * @property int $bonemass
 * @property int $height
 * @property int $weight
 * @property int $fat
 * @property int $neck
 * @property int $rightarm
 * @property int $leftarm
 * @property int $wrist
 * @property int $core
 * @property int $hip
 * @property int $thorax
 * @property int $righthigh
 * @property int $lefthigh
 * @property int $rightcalve
 * @property int $leftcalve
 * @property \Cake\I18n\FrozenTime $creationdate
 * @property int $subscriber_id
 *
 * @property \App\Model\Entity\Subscriber $subscriber
 */
class Measurement extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
