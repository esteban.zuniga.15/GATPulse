<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Training Entity
 *
 * @property int $id
 * @property string $name
 * @property int $instructor_id
 * @property int $gymprofile_id
 * @property int $capacity
 * @property int $schedule_id
 *
 * @property \App\Model\Entity\Instructor $instructor
 * @property \App\Model\Entity\Gymprofile $gymprofile
 * @property \App\Model\Entity\Schedule $schedule
 */
class Training extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
