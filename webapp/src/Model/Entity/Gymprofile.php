<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gymprofile Entity
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $address
 * @property string $state
 * @property string $country
 * @property string $postalcode
 * @property string $payment
 * @property int $schedule_id
 *
 * @property \App\Model\Entity\Schedule $schedule
 * @property \App\Model\Entity\Subscriber[] $subscribers
 * @property \App\Model\Entity\Training[] $trainings
 * @property \App\Model\Entity\User[] $users
 */
class Gymprofile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
