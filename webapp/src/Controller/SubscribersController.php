<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Subscribers Controller
 *
 * @property \App\Model\Table\SubscribersTable $Subscribers
 *
 * @method \App\Model\Entity\Subscriber[] paginate($object = null, array $settings = [])
 */
class SubscribersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Gymprofiles', 'Instructors']
        ];
        $subscribers = $this->paginate($this->Subscribers);

        $this->set(compact('subscribers'));
        $this->set('_serialize', ['subscribers']);
    }

    /**
     * CardIndex method
     *
     * @return \Cake\Http\Response|void
     */
    public function cardindex()
    {
        $this->paginate = [
            'contain' => ['Gymprofiles', 'Instructors']
        ];
        $subscribers = $this->paginate($this->Subscribers);

        $this->set(compact('subscribers'));
        $this->set('_serialize', ['subscribers']);
        $this->render('cardindex');
    }

    /**
     * View method
     *
     * @param string|null $id Subscriber id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      $this->autoRender = false;
      $subscriber = $this->Subscribers->get($id, [
        'contain' => ['Gymprofiles', 'Instructors', 'Measurements', 'Routines']
      ]);
      if ($this->request->is(['patch', 'post', 'put'])) {
          $subscriber = $this->Subscribers->patchEntity($subscriber, $this->request->getData());
          if ($this->Subscribers->save($subscriber)) {
              $this->Flash->success(__('El subscriptor ha sido guardado.'));

              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('El subscriptor no pudo ser guardado.'));
      }
      $gymprofiles = $this->Subscribers->Gymprofiles->find('list', ['limit' => 200]);
      $instructors = $this->Subscribers->Instructors->find('list', ['limit' => 200]);
      $this->set(compact('subscriber', 'gymprofiles', 'instructors'));
      $this->set('_serialize', ['subscriber']);
      $this->render('view');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        parent::initialize();
        $this->loadComponent('UploadedFilesHandler');
        $subscriber = $this->Subscribers->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['joined'] = date("Y-m-d");

            $this->request->data['birthday'] = $this->request->data['customBirthday'];

            $this->request->data['photo'] = $this->UploadedFilesHandler->uploadCroppiePhoto();

            $subscriber = $this->Subscribers->patchEntity($subscriber, $this->request->getData());
            if ($this->Subscribers->save($subscriber)) {
                $this->Flash->success(__('El subscriptor ha sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El subscriptor no pudo ser guardado.'));
        }
        $gymprofiles = $this->Subscribers->Gymprofiles->find('list', ['limit' => 200]);
        $instructors = $this->Subscribers->Instructors->find('list', ['limit' => 200]);
        $this->set(compact('subscriber', 'gymprofiles', 'instructors'));
        $this->set('_serialize', ['subscriber']);
    }


    /**
     * changePicture method
     *
     * @param string|null $id Subscriber id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function changePicture($id = null)
    {
      $this->autoRender = false;
      $subscriber = $this->Subscribers->get($id);
      if ($this->request->is(['patch', 'post', 'put'])) {
          $this->loadComponent('UploadedFilesHandler');
          $this->request->data['photo'] = $this->UploadedFilesHandler->uploadCroppiePhoto();
          $subscriber = $this->Subscribers->patchEntity($subscriber, $this->request->getData());
          if ($this->Subscribers->save($subscriber)) {
              $this->Flash->success(__('El subscriptor ha sido guardado.'));

              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('El subscriptor no pudo ser guardado.'));
      }
      $gymprofiles = $this->Subscribers->Gymprofiles->find('list', ['limit' => 200]);
      $instructors = $this->Subscribers->Instructors->find('list', ['limit' => 200]);
      $this->set(compact('subscriber', 'gymprofiles', 'instructors'));
      $this->set('_serialize', ['subscriber']);
      $this->render('changepicture');
    }


    /**
     * Delete method
     *
     * @param string|null $id Subscriber id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subscriber = $this->Subscribers->get($id);
        if ($this->Subscribers->delete($subscriber)) {
            $this->Flash->success(__('El subscriptor ha sido borrado'));
        } else {
            $this->Flash->error(__('El subscriptor no ha podido ser borrado'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
