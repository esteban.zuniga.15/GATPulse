<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Gymprofiles Controller
 *
 * @property \App\Model\Table\GymprofilesTable $Gymprofiles
 *
 * @method \App\Model\Entity\Gymprofile[] paginate($object = null, array $settings = [])
 */
class GymprofilesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Schedules']
        ];
        $gymprofiles = $this->paginate($this->Gymprofiles);

        $this->set(compact('gymprofiles'));
        $this->set('_serialize', ['gymprofiles']);
    }

    /**
     * View method
     *
     * @param string|null $id Gymprofile id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gymprofile = $this->Gymprofiles->get($id, [
            'contain' => ['Schedules', 'Subscribers', 'Trainings', 'Users']
        ]);

        $this->set('gymprofile', $gymprofile);
        $this->set('_serialize', ['gymprofile']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gymprofile = $this->Gymprofiles->newEntity();
        if ($this->request->is('post')) {
            $gymprofile = $this->Gymprofiles->patchEntity($gymprofile, $this->request->getData());
            if ($this->Gymprofiles->save($gymprofile)) {
                $this->Flash->success(__('The gymprofile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gymprofile could not be saved. Please, try again.'));
        }
        $schedules = $this->Gymprofiles->Schedules->find('list', ['limit' => 200]);
        $this->set(compact('gymprofile', 'schedules'));
        $this->set('_serialize', ['gymprofile']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gymprofile id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gymprofile = $this->Gymprofiles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gymprofile = $this->Gymprofiles->patchEntity($gymprofile, $this->request->getData());
            if ($this->Gymprofiles->save($gymprofile)) {
                $this->Flash->success(__('The gymprofile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gymprofile could not be saved. Please, try again.'));
        }
        $schedules = $this->Gymprofiles->Schedules->find('list', ['limit' => 200]);
        $this->set(compact('gymprofile', 'schedules'));
        $this->set('_serialize', ['gymprofile']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gymprofile id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gymprofile = $this->Gymprofiles->get($id);
        if ($this->Gymprofiles->delete($gymprofile)) {
            $this->Flash->success(__('The gymprofile has been deleted.'));
        } else {
            $this->Flash->error(__('The gymprofile could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
