<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Exercisetypes Controller
 *
 * @property \App\Model\Table\ExercisetypesTable $Exercisetypes
 *
 * @method \App\Model\Entity\Exercisetype[] paginate($object = null, array $settings = [])
 */
class ExercisetypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $exercisetypes = $this->paginate($this->Exercisetypes);

        $this->set(compact('exercisetypes'));
        $this->set('_serialize', ['exercisetypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Exercisetype id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $exercisetype = $this->Exercisetypes->get($id, [
            'contain' => []
        ]);

        $this->set('exercisetype', $exercisetype);
        $this->set('_serialize', ['exercisetype']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $exercisetype = $this->Exercisetypes->newEntity();
        if ($this->request->is('post')) {
            $exercisetype = $this->Exercisetypes->patchEntity($exercisetype, $this->request->getData());
            if ($this->Exercisetypes->save($exercisetype)) {
                $this->Flash->success(__('The exercisetype has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The exercisetype could not be saved. Please, try again.'));
        }
        $this->set(compact('exercisetype'));
        $this->set('_serialize', ['exercisetype']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Exercisetype id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $exercisetype = $this->Exercisetypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $exercisetype = $this->Exercisetypes->patchEntity($exercisetype, $this->request->getData());
            if ($this->Exercisetypes->save($exercisetype)) {
                $this->Flash->success(__('The exercisetype has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The exercisetype could not be saved. Please, try again.'));
        }
        $this->set(compact('exercisetype'));
        $this->set('_serialize', ['exercisetype']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Exercisetype id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $exercisetype = $this->Exercisetypes->get($id);
        if ($this->Exercisetypes->delete($exercisetype)) {
            $this->Flash->success(__('The exercisetype has been deleted.'));
        } else {
            $this->Flash->error(__('The exercisetype could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
