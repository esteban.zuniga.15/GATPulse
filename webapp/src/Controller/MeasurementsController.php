<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Measurements Controller
 *
 * @property \App\Model\Table\MeasurementsTable $Measurements
 *
 * @method \App\Model\Entity\Measurement[] paginate($object = null, array $settings = [])
 */
class MeasurementsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Subscribers']
        ];
        $measurements = $this->paginate($this->Measurements);

        $this->set(compact('measurements'));
        $this->set('_serialize', ['measurements']);
    }

    /**
     * View method
     *
     * @param string|null $id Measurement id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $measurement = $this->Measurements->get($id, [
            'contain' => ['Subscribers']
        ]);

        $this->set('measurement', $measurement);
        $this->set('_serialize', ['measurement']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $measurement = $this->Measurements->newEntity();
        if ($this->request->is('post')) {
            $measurement = $this->Measurements->patchEntity($measurement, $this->request->getData());
            if ($this->Measurements->save($measurement)) {
                $this->Flash->success(__('The measurement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The measurement could not be saved. Please, try again.'));
        }
        $subscribers = $this->Measurements->Subscribers->find('list', ['limit' => 200]);
        $this->set(compact('measurement', 'subscribers'));
        $this->set('_serialize', ['measurement']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Measurement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $measurement = $this->Measurements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $measurement = $this->Measurements->patchEntity($measurement, $this->request->getData());
            if ($this->Measurements->save($measurement)) {
                $this->Flash->success(__('The measurement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The measurement could not be saved. Please, try again.'));
        }
        $subscribers = $this->Measurements->Subscribers->find('list', ['limit' => 200]);
        $this->set(compact('measurement', 'subscribers'));
        $this->set('_serialize', ['measurement']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Measurement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $measurement = $this->Measurements->get($id);
        if ($this->Measurements->delete($measurement)) {
            $this->Flash->success(__('The measurement has been deleted.'));
        } else {
            $this->Flash->error(__('The measurement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
