<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Gymprofile $gymprofile
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Gymprofile'), ['action' => 'edit', $gymprofile->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gymprofile'), ['action' => 'delete', $gymprofile->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gymprofile->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Schedules'), ['controller' => 'Schedules', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Schedule'), ['controller' => 'Schedules', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subscribers'), ['controller' => 'Subscribers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subscriber'), ['controller' => 'Subscribers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Trainings'), ['controller' => 'Trainings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Training'), ['controller' => 'Trainings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gymprofiles view large-9 medium-8 columns content">
    <h3><?= h($gymprofile->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($gymprofile->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Url') ?></th>
            <td><?= h($gymprofile->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($gymprofile->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= h($gymprofile->state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= h($gymprofile->country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postalcode') ?></th>
            <td><?= h($gymprofile->postalcode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Payment') ?></th>
            <td><?= h($gymprofile->payment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Schedule') ?></th>
            <td><?= $gymprofile->has('schedule') ? $this->Html->link($gymprofile->schedule->id, ['controller' => 'Schedules', 'action' => 'view', $gymprofile->schedule->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($gymprofile->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Subscribers') ?></h4>
        <?php if (!empty($gymprofile->subscribers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Lastnames') ?></th>
                <th scope="col"><?= __('Birthday') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Phone') ?></th>
                <th scope="col"><?= __('Emergencyphone') ?></th>
                <th scope="col"><?= __('Photo') ?></th>
                <th scope="col"><?= __('Gymprofile Id') ?></th>
                <th scope="col"><?= __('Joined') ?></th>
                <th scope="col"><?= __('Instructor Id') ?></th>
                <th scope="col"><?= __('Sex') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($gymprofile->subscribers as $subscribers): ?>
            <tr>
                <td><?= h($subscribers->id) ?></td>
                <td><?= h($subscribers->name) ?></td>
                <td><?= h($subscribers->lastnames) ?></td>
                <td><?= h($subscribers->birthday) ?></td>
                <td><?= h($subscribers->email) ?></td>
                <td><?= h($subscribers->phone) ?></td>
                <td><?= h($subscribers->emergencyphone) ?></td>
                <td><?= h($subscribers->photo) ?></td>
                <td><?= h($subscribers->gymprofile_id) ?></td>
                <td><?= h($subscribers->joined) ?></td>
                <td><?= h($subscribers->instructor_id) ?></td>
                <td><?= h($subscribers->sex) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Subscribers', 'action' => 'view', $subscribers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Subscribers', 'action' => 'edit', $subscribers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Subscribers', 'action' => 'delete', $subscribers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subscribers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Trainings') ?></h4>
        <?php if (!empty($gymprofile->trainings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Instructor Id') ?></th>
                <th scope="col"><?= __('Gymprofile Id') ?></th>
                <th scope="col"><?= __('Capacity') ?></th>
                <th scope="col"><?= __('Schedule Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($gymprofile->trainings as $trainings): ?>
            <tr>
                <td><?= h($trainings->id) ?></td>
                <td><?= h($trainings->name) ?></td>
                <td><?= h($trainings->instructor_id) ?></td>
                <td><?= h($trainings->gymprofile_id) ?></td>
                <td><?= h($trainings->capacity) ?></td>
                <td><?= h($trainings->schedule_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Trainings', 'action' => 'view', $trainings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Trainings', 'action' => 'edit', $trainings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Trainings', 'action' => 'delete', $trainings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trainings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($gymprofile->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Gymprofile Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($gymprofile->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->type) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->gymprofile_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
