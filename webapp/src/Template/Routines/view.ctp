<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Routine $routine
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Routine'), ['action' => 'edit', $routine->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Routine'), ['action' => 'delete', $routine->id], ['confirm' => __('Are you sure you want to delete # {0}?', $routine->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Routines'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Routine'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subscribers'), ['controller' => 'Subscribers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subscriber'), ['controller' => 'Subscribers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sets'), ['controller' => 'Sets', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Set'), ['controller' => 'Sets', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="routines view large-9 medium-8 columns content">
    <h3><?= h($routine->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($routine->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subscriber') ?></th>
            <td><?= $routine->has('subscriber') ? $this->Html->link($routine->subscriber->name, ['controller' => 'Subscribers', 'action' => 'view', $routine->subscriber->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creationtimestamp') ?></th>
            <td><?= h($routine->creationtimestamp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($routine->id) ?></td>
        </tr>
    </table>

    <?php

              foreach ($exercisesArray as $exercise):?>
              <div class="exercise <?php echo 'setType'.$exercise['type_id']?>">
                <?php

              echo $exercise['name'];
              ?>
            </div>
            <?php endforeach;
            ?>
    </div>
</div>
