<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $routine->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $routine->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Routines'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Subscribers'), ['controller' => 'Subscribers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subscriber'), ['controller' => 'Subscribers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sets'), ['controller' => 'Sets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Set'), ['controller' => 'Sets', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="routines form large-9 medium-8 columns content">
    <?= $this->Form->create($routine) ?>
    <fieldset>
        <legend><?= __('Edit Routine') ?></legend>
        <?php
            echo $this->Form->control('type');
            echo $this->Form->control('subscriber_id', ['options' => $subscribers, 'empty' => true]);
            echo $this->Form->control('creationtimestamp');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
