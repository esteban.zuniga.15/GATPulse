<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Set $set
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Set'), ['action' => 'edit', $set->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Set'), ['action' => 'delete', $set->id], ['confirm' => __('Are you sure you want to delete # {0}?', $set->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Set'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Routines'), ['controller' => 'Routines', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Routine'), ['controller' => 'Routines', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Exercises'), ['controller' => 'Exercises', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Exercise'), ['controller' => 'Exercises', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sets view large-9 medium-8 columns content">
    <h3><?= h($set->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Routine') ?></th>
            <td><?= $set->has('routine') ? $this->Html->link($set->routine->id, ['controller' => 'Routines', 'action' => 'view', $set->routine->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($set->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($set->type) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Exercises') ?></h4>
        <?php if (!empty($set->exercises)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Instructions') ?></th>
                <th scope="col"><?= __('Position') ?></th>
                <th scope="col"><?= __('Type Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($set->exercises as $exercises): ?>
            <tr>
                <td><?= h($exercises->id) ?></td>
                <td><?= h($exercises->name) ?></td>
                <td><?= h($exercises->instructions) ?></td>
                <td><?= h($exercises->position) ?></td>
                <td><?= h($exercises->type_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Exercises', 'action' => 'view', $exercises->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Exercises', 'action' => 'edit', $exercises->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Exercises', 'action' => 'delete', $exercises->id], ['confirm' => __('Are you sure you want to delete # {0}?', $exercises->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
