<?=
$this->Html->link(
    'Homepage',
    '/pages/home',
    ['class' => 'button', 'target' => '_blank']
);
?>

<br>

<?=
$this->Html->link(
    'Subscriptores',
    '/subscribers',
    ['class' => 'button', 'target' => '_blank']
);
?>

<br>

<?=
$this->Html->link(
    'Instructores',
    '/instructors',
    ['class' => 'button', 'target' => '_blank']
);
?>

<br>

<?=
$this->Html->link(
    'Entrenamientos',
    '/trainings',
    ['class' => 'button', 'target' => '_blank']
);
?>

<br>

<?=
$this->Html->link(
    'Rutinas',
    '/routines',
    ['class' => 'button', 'target' => '_blank']
);
?>

<br>

<?=
$this->Html->link(
    'Medidas',
    '/measurements',
    ['class' => 'button', 'target' => '_blank']
);
?>

<br>

<?=
$this->Html->link(
    'Ejercicios',
    '/exercises',
    ['class' => 'button', 'target' => '_blank']
);
?>

<br>

<?=
$this->Html->link(
    'Datos de este gimnasio',
    '/gymprofiles',
    ['class' => 'button', 'target' => '_blank']
);
?>
