<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Trainings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Schedules'), ['controller' => 'Schedules', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Schedule'), ['controller' => 'Schedules', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="trainings form large-9 medium-8 columns content">
    <?= $this->Form->create('Training', ['controller' => 'Trainings', 'url' => 'trainings/add']) ?>
    <fieldset>
        <legend><?= __('Add Training') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('instructor_id', ['options' => $instructors, 'empty' => true]);
            echo $this->Form->control('gymprofile_id', ['options' => $gymprofiles, 'empty' => true]);
            echo $this->Form->control('capacity');
            echo $this->Form->control('schedule_id', ['options' => $schedules, 'empty' => true]);
        ?>
    </fieldset>
</div>

<div class="schedules form large-9 medium-8 columns content">
    <?= $this->Form->create('Schedule', ['controller' => 'Schedules', 'url' => 'schedules/add']) ?>
    <fieldset>
        <legend><?= __('Add Schedule') ?></legend>
        <?php
            echo $this->Form->control('mon', ['type' => 'checkbox']);
            echo $this->Form->control('tue', ['type' => 'checkbox']);
            echo $this->Form->control('wed', ['type' => 'checkbox']);
            echo $this->Form->control('thu', ['type' => 'checkbox']);
            echo $this->Form->control('fri', ['type' => 'checkbox']);
            echo $this->Form->control('sat', ['type' => 'checkbox']);
            echo $this->Form->control('sun', ['type' => 'checkbox']);
            echo $this->Form->control('starthour');
            echo $this->Form->control('endhour');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
