<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Training $training
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Training'), ['action' => 'edit', $training->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Training'), ['action' => 'delete', $training->id], ['confirm' => __('Are you sure you want to delete # {0}?', $training->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Trainings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Training'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Schedules'), ['controller' => 'Schedules', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Schedule'), ['controller' => 'Schedules', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="trainings view large-9 medium-8 columns content">
    <h3><?= h($training->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($training->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Instructor') ?></th>
            <td><?= $training->has('instructor') ? $this->Html->link($training->instructor->name, ['controller' => 'Instructors', 'action' => 'view', $training->instructor->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gymprofile') ?></th>
            <td><?= $training->has('gymprofile') ? $this->Html->link($training->gymprofile->name, ['controller' => 'Gymprofiles', 'action' => 'view', $training->gymprofile->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Schedule') ?></th>
            <td><?= $training->has('schedule') ? $this->Html->link($training->schedule->id, ['controller' => 'Schedules', 'action' => 'view', $training->schedule->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($training->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Capacity') ?></th>
            <td><?= $this->Number->format($training->capacity) ?></td>
        </tr>
    </table>
</div>
