<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $training->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $training->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Trainings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Schedules'), ['controller' => 'Schedules', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Schedule'), ['controller' => 'Schedules', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="trainings form large-9 medium-8 columns content">
    <?= $this->Form->create($training) ?>
    <fieldset>
        <legend><?= __('Edit Training') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('instructor_id', ['options' => $instructors, 'empty' => true]);
            echo $this->Form->control('gymprofile_id', ['options' => $gymprofiles, 'empty' => true]);
            echo $this->Form->control('capacity');
            echo $this->Form->control('schedule_id', ['options' => $schedules, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
