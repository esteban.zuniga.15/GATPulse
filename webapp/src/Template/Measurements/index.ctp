<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Measurement[]|\Cake\Collection\CollectionInterface $measurements
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Measurement'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Subscribers'), ['controller' => 'Subscribers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subscriber'), ['controller' => 'Subscribers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="measurements index large-9 medium-8 columns content">
    <h3><?= __('Measurements') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('metabolicage') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bmr') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bonemass') ?></th>
                <th scope="col"><?= $this->Paginator->sort('height') ?></th>
                <th scope="col"><?= $this->Paginator->sort('weight') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fat') ?></th>
                <th scope="col"><?= $this->Paginator->sort('neck') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rightarm') ?></th>
                <th scope="col"><?= $this->Paginator->sort('leftarm') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wrist') ?></th>
                <th scope="col"><?= $this->Paginator->sort('core') ?></th>
                <th scope="col"><?= $this->Paginator->sort('hip') ?></th>
                <th scope="col"><?= $this->Paginator->sort('thorax') ?></th>
                <th scope="col"><?= $this->Paginator->sort('righthigh') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lefthigh') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rightcalve') ?></th>
                <th scope="col"><?= $this->Paginator->sort('leftcalve') ?></th>
                <th scope="col"><?= $this->Paginator->sort('creationdate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subscriber_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($measurements as $measurement): ?>
            <tr>
                <td><?= $this->Number->format($measurement->id) ?></td>
                <td><?= $this->Number->format($measurement->metabolicage) ?></td>
                <td><?= $this->Number->format($measurement->bmr) ?></td>
                <td><?= $this->Number->format($measurement->bonemass) ?></td>
                <td><?= $this->Number->format($measurement->height) ?></td>
                <td><?= $this->Number->format($measurement->weight) ?></td>
                <td><?= $this->Number->format($measurement->fat) ?></td>
                <td><?= $this->Number->format($measurement->neck) ?></td>
                <td><?= $this->Number->format($measurement->rightarm) ?></td>
                <td><?= $this->Number->format($measurement->leftarm) ?></td>
                <td><?= $this->Number->format($measurement->wrist) ?></td>
                <td><?= $this->Number->format($measurement->core) ?></td>
                <td><?= $this->Number->format($measurement->hip) ?></td>
                <td><?= $this->Number->format($measurement->thorax) ?></td>
                <td><?= $this->Number->format($measurement->righthigh) ?></td>
                <td><?= $this->Number->format($measurement->lefthigh) ?></td>
                <td><?= $this->Number->format($measurement->rightcalve) ?></td>
                <td><?= $this->Number->format($measurement->leftcalve) ?></td>
                <td><?= h($measurement->creationdate) ?></td>
                <td><?= $measurement->has('subscriber') ? $this->Html->link($measurement->subscriber->name, ['controller' => 'Subscribers', 'action' => 'view', $measurement->subscriber->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $measurement->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $measurement->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $measurement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measurement->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
