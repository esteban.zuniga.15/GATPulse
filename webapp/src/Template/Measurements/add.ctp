<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Measurements'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Subscribers'), ['controller' => 'Subscribers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Subscriber'), ['controller' => 'Subscribers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="measurements form large-9 medium-8 columns content">
    <?= $this->Form->create($measurement) ?>
    <fieldset>
        <legend><?= __('Add Measurement') ?></legend>
        <?php
            echo $this->Form->control('metabolicage');
            echo $this->Form->control('bmr');
            echo $this->Form->control('bonemass');
            echo $this->Form->control('height');
            echo $this->Form->control('weight');
            echo $this->Form->control('fat');
            echo $this->Form->control('neck');
            echo $this->Form->control('rightarm');
            echo $this->Form->control('leftarm');
            echo $this->Form->control('wrist');
            echo $this->Form->control('core');
            echo $this->Form->control('hip');
            echo $this->Form->control('thorax');
            echo $this->Form->control('righthigh');
            echo $this->Form->control('lefthigh');
            echo $this->Form->control('rightcalve');
            echo $this->Form->control('leftcalve');
            echo $this->Form->control('creationdate', ['empty' => true]);
            echo $this->Form->control('subscriber_id', ['options' => $subscribers, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
