<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Measurement $measurement
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Measurement'), ['action' => 'edit', $measurement->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Measurement'), ['action' => 'delete', $measurement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measurement->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Measurements'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Measurement'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Subscribers'), ['controller' => 'Subscribers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subscriber'), ['controller' => 'Subscribers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="measurements view large-9 medium-8 columns content">
    <h3><?= h($measurement->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Subscriber') ?></th>
            <td><?= $measurement->has('subscriber') ? $this->Html->link($measurement->subscriber->name, ['controller' => 'Subscribers', 'action' => 'view', $measurement->subscriber->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($measurement->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Metabolicage') ?></th>
            <td><?= $this->Number->format($measurement->metabolicage) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bmr') ?></th>
            <td><?= $this->Number->format($measurement->bmr) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bonemass') ?></th>
            <td><?= $this->Number->format($measurement->bonemass) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Height') ?></th>
            <td><?= $this->Number->format($measurement->height) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Weight') ?></th>
            <td><?= $this->Number->format($measurement->weight) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fat') ?></th>
            <td><?= $this->Number->format($measurement->fat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Neck') ?></th>
            <td><?= $this->Number->format($measurement->neck) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rightarm') ?></th>
            <td><?= $this->Number->format($measurement->rightarm) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Leftarm') ?></th>
            <td><?= $this->Number->format($measurement->leftarm) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Wrist') ?></th>
            <td><?= $this->Number->format($measurement->wrist) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Core') ?></th>
            <td><?= $this->Number->format($measurement->core) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hip') ?></th>
            <td><?= $this->Number->format($measurement->hip) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Thorax') ?></th>
            <td><?= $this->Number->format($measurement->thorax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Righthigh') ?></th>
            <td><?= $this->Number->format($measurement->righthigh) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lefthigh') ?></th>
            <td><?= $this->Number->format($measurement->lefthigh) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rightcalve') ?></th>
            <td><?= $this->Number->format($measurement->rightcalve) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Leftcalve') ?></th>
            <td><?= $this->Number->format($measurement->leftcalve) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creationdate') ?></th>
            <td><?= h($measurement->creationdate) ?></td>
        </tr>
    </table>
</div>
