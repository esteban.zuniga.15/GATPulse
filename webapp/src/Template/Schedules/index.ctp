<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Schedule[]|\Cake\Collection\CollectionInterface $schedules
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Schedule'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Trainings'), ['controller' => 'Trainings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Training'), ['controller' => 'Trainings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="schedules index large-9 medium-8 columns content">
    <h3><?= __('Schedules') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mon') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tue') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('thu') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fri') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sat') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sun') ?></th>
                <th scope="col"><?= $this->Paginator->sort('starthour') ?></th>
                <th scope="col"><?= $this->Paginator->sort('endhour') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($schedules as $schedule): ?>
            <tr>
                <td><?= $this->Number->format($schedule->id) ?></td>
                <td><?= h($schedule->mon) ?></td>
                <td><?= h($schedule->tue) ?></td>
                <td><?= h($schedule->wed) ?></td>
                <td><?= h($schedule->thu) ?></td>
                <td><?= h($schedule->fri) ?></td>
                <td><?= h($schedule->sat) ?></td>
                <td><?= h($schedule->sun) ?></td>
                <td><?= h($schedule->starthour) ?></td>
                <td><?= h($schedule->endhour) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $schedule->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $schedule->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $schedule->id], ['confirm' => __('Are you sure you want to delete # {0}?', $schedule->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
