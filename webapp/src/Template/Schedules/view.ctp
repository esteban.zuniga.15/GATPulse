<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Schedule $schedule
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Schedule'), ['action' => 'edit', $schedule->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Schedule'), ['action' => 'delete', $schedule->id], ['confirm' => __('Are you sure you want to delete # {0}?', $schedule->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Schedules'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Schedule'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Trainings'), ['controller' => 'Trainings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Training'), ['controller' => 'Trainings', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="schedules view large-9 medium-8 columns content">
    <h3><?= h($schedule->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Starthour') ?></th>
            <td><?= h($schedule->starthour) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Endhour') ?></th>
            <td><?= h($schedule->endhour) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($schedule->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mon') ?></th>
            <td><?= $schedule->mon ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tue') ?></th>
            <td><?= $schedule->tue ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Wed') ?></th>
            <td><?= $schedule->wed ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Thu') ?></th>
            <td><?= $schedule->thu ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fri') ?></th>
            <td><?= $schedule->fri ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sat') ?></th>
            <td><?= $schedule->sat ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sun') ?></th>
            <td><?= $schedule->sun ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Gymprofiles') ?></h4>
        <?php if (!empty($schedule->gymprofiles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('State') ?></th>
                <th scope="col"><?= __('Country') ?></th>
                <th scope="col"><?= __('Postalcode') ?></th>
                <th scope="col"><?= __('Payment') ?></th>
                <th scope="col"><?= __('Schedule Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($schedule->gymprofiles as $gymprofiles): ?>
            <tr>
                <td><?= h($gymprofiles->id) ?></td>
                <td><?= h($gymprofiles->name) ?></td>
                <td><?= h($gymprofiles->url) ?></td>
                <td><?= h($gymprofiles->address) ?></td>
                <td><?= h($gymprofiles->state) ?></td>
                <td><?= h($gymprofiles->country) ?></td>
                <td><?= h($gymprofiles->postalcode) ?></td>
                <td><?= h($gymprofiles->payment) ?></td>
                <td><?= h($gymprofiles->schedule_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Gymprofiles', 'action' => 'view', $gymprofiles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Gymprofiles', 'action' => 'edit', $gymprofiles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Gymprofiles', 'action' => 'delete', $gymprofiles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gymprofiles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Instructors') ?></h4>
        <?php if (!empty($schedule->instructors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Trainings') ?></th>
                <th scope="col"><?= __('Schedule Id') ?></th>
                <th scope="col"><?= __('Subscribers') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($schedule->instructors as $instructors): ?>
            <tr>
                <td><?= h($instructors->id) ?></td>
                <td><?= h($instructors->name) ?></td>
                <td><?= h($instructors->trainings) ?></td>
                <td><?= h($instructors->schedule_id) ?></td>
                <td><?= h($instructors->subscribers) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Instructors', 'action' => 'view', $instructors->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Instructors', 'action' => 'edit', $instructors->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Instructors', 'action' => 'delete', $instructors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $instructors->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Trainings') ?></h4>
        <?php if (!empty($schedule->trainings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Instructor Id') ?></th>
                <th scope="col"><?= __('Gymprofile Id') ?></th>
                <th scope="col"><?= __('Capacity') ?></th>
                <th scope="col"><?= __('Schedule Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($schedule->trainings as $trainings): ?>
            <tr>
                <td><?= h($trainings->id) ?></td>
                <td><?= h($trainings->name) ?></td>
                <td><?= h($trainings->instructor_id) ?></td>
                <td><?= h($trainings->gymprofile_id) ?></td>
                <td><?= h($trainings->capacity) ?></td>
                <td><?= h($trainings->schedule_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Trainings', 'action' => 'view', $trainings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Trainings', 'action' => 'edit', $trainings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Trainings', 'action' => 'delete', $trainings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trainings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
