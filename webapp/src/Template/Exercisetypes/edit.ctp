<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $exercisetype->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $exercisetype->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Exercisetypes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="exercisetypes form large-9 medium-8 columns content">
    <?= $this->Form->create($exercisetype) ?>
    <fieldset>
        <legend><?= __('Edit Exercisetype') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
