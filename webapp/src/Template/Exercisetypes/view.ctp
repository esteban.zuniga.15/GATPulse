<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Exercisetype $exercisetype
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Exercisetype'), ['action' => 'edit', $exercisetype->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Exercisetype'), ['action' => 'delete', $exercisetype->id], ['confirm' => __('Are you sure you want to delete # {0}?', $exercisetype->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Exercisetypes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Exercisetype'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="exercisetypes view large-9 medium-8 columns content">
    <h3><?= h($exercisetype->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($exercisetype->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($exercisetype->id) ?></td>
        </tr>
    </table>
</div>
