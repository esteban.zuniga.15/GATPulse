<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Subscriber $subscriber
  */
?>

<?php echo $this->Html->css('GeneralPurpose/hidden.css'); ?>
<?php echo $this->Html->css('GeneralPurpose/inputDateCleaner.css'); ?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Subscriber'), ['action' => 'edit', $subscriber->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subscriber'), ['action' => 'delete', $subscriber->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subscriber->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subscribers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subscriber'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Measurements'), ['controller' => 'Measurements', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Measurement'), ['controller' => 'Measurements', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Routines'), ['controller' => 'Routines', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Routine'), ['controller' => 'Routines', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subscribers view large-9 medium-8 columns content">
    <h3><?= h($subscriber->name ." ". $subscriber->lastnames) ?></h3>
    <?= $this->Form->create($subscriber) ?>
    <fieldset>
      <?php
      echo $this->Form->hidden('idlegal');
      echo $this->Form->input('Foto de perfil', ['id' => 'upload','type' => 'file']); ?>
      <div class="" id="uploadCirclePhoto"></div>
      <input type="hidden" id="imagebase64" name="imagebase64">
    </fieldset>
    <?= $this->Form->button(__('Guardar'),['id'=>'mainSave','class'=>'uploadCroppie']) ?>
    <?= $this->Form->end() ?>
</div>

<?php echo $this->Html->script('General/editActions.js'); ?>
<?php echo $this->Html->css('GeneralPurpose/croppie.css'); ?>
<?php echo $this->Html->script('General/croppie.js'); ?>
<?php echo $this->Html->script('General/uploadPhotoSelector.js'); ?>
