<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php echo $this->Html->css('GeneralPurpose/hidden.css'); ?>
<?php echo $this->Html->css('GeneralPurpose/inputDateCleaner.css'); ?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Subscribers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Measurements'), ['controller' => 'Measurements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Measurement'), ['controller' => 'Measurements', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Routines'), ['controller' => 'Routines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Routine'), ['controller' => 'Routines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subscribers form large-9 medium-8 columns content">
    <?php
    //echo $_SERVER['DOCUMENT_ROOT'];
    ?>
    <?= $this->Form->create($subscriber, ['type' => 'file','id'=>'form']) ?>
    <fieldset>
        <legend><?= __('Agregar subscriptor') ?></legend>
        <?php
            echo $this->Form->control('name',['label' => 'Nombre']);
            echo $this->Form->control('lastnames',['label' => 'Apellidos']);
            //echo $this->Form->control('birthday');
            echo $this->Form->label('birthday','Fecha de nacimiento');?>
            <input type='date' name='customBirthday'>
            <?php

            //echo $this->Form->date('customBirthday',['label' => 'Fecha de nacimiento']);
            echo $this->Form->control('email',['label' => 'Correo electronico']);
            echo $this->Form->control('phone',['label' => 'Número de teléfono']);
            echo $this->Form->control('emergencyphone',['label' => 'Teléfono de emergencia']);
            //echo $this->Form->control('photo');?>
            <?php echo $this->Form->label('upload', 'Foto de prefil');?>

            <div class='fileUpload btn'>
              <span> Subir </span>
              <input type='file' id='upload' class='upload'>
            <?php //echo $this->Form->input('Foto de perfil', ['id' => 'upload','type' => 'file','class'=>'upload']); ?>
            </div>
            <div class="" id="uploadCirclePhoto"></div>
            <input type="hidden" id="imagebase64" name="imagebase64">

            <?php
            //echo $this->Form->control('gymprofile_id', ['options' => $gymprofiles, 'empty' => true]);
            //echo $this->Form->control('joined');
            echo $this->Form->control('instructor_id', ['options' => $instructors, 'empty' => true]);

            echo $this->Form->label('sex','Sexo');
            $genders = ['F' => 'Femenino', 'M' => 'Masculino', 'O' => 'Otro'];
            echo $this->Form->select('sex', $genders, ['default' => 'm']);

            echo $this->Form->control('idlegal',['label' => 'Identificación legal']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Agregar'), ['class'=>'uploadCroppie']) ?>
    <?= $this->Form->end() ?>
</div>

<?php echo $this->Html->css('GeneralPurpose/croppie.css'); ?>
<?php echo $this->Html->script('General/croppie.js'); ?>
<?php echo $this->Html->script('General/uploadPhotoSelector.js'); ?>
