<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Subscriber $subscriber
  */
?>

<?php echo $this->Html->css('GeneralPurpose/hidden.css'); ?>
<?php echo $this->Html->css('GeneralPurpose/inputDateCleaner.css'); ?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Subscriber'), ['action' => 'edit', $subscriber->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subscriber'), ['action' => 'delete', $subscriber->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subscriber->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Subscribers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Subscriber'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Measurements'), ['controller' => 'Measurements', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Measurement'), ['controller' => 'Measurements', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Routines'), ['controller' => 'Routines', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Routine'), ['controller' => 'Routines', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="subscribers view large-9 medium-8 columns content">
    <h3><?= h($subscriber->name ." ". $subscriber->lastnames) ?></h3>
    <?= $this->Form->create($subscriber) ?>
    <fieldset>
        <?php
            echo $this->Form->control('name',['label'=>'Nombre','readonly']);
            echo $this->Form->control('lastnames',['label'=>'Apellidos','readonly']);
            echo $this->Form->label('birthday','Fecha de nacimiento');?>
            <input type='date' name='birthday' value='<?php echo date("Y-m-d", strtotime($subscriber->birthday)) ?>' readonly>
            <?php
            echo $this->Form->control('email',['label'=>'Correo electrónico','readonly']);
            echo $this->Form->control('phone',['label'=>'Teléfono','readonly']);
            echo $this->Form->control('emergencyphone',['label'=>'Teléfono de emergencia','readonly']);
            echo $this->Form->label('upload','Foto de perfil');?>
            <br>
            <?php
            echo $this->Html->image("/uploads/subsphotos/".$subscriber->photo, ['alt' => 'Foto de perfil de '.$subscriber->name]);?>

            <br>
            <?php
            echo $this->Html->link(
                'Cambiar',
                '/Subscribers/changepicture/'.$subscriber->id,
                ['class' => 'btn', 'target' => '_blank']
            );?>
            <br>
            <?php
            //echo $this->Form->control('joined',['label'=>'Fecha de ingreso','readonly']);
            echo $this->Form->label('joined','Fecha de ingreso');?>
            <input type='date' name='joined' value='<?php echo date("Y-m-d", strtotime($subscriber->joined)) ?>' readonly>
            <?php
            $subscriber->has('instructor') ? $this->Html->link($subscriber->instructor->name, ['controller' => 'Instructors', 'action' => 'view', $subscriber->instructor->id]) : '' ;
            //find a way to generate this upcoming line on edit
            //echo $this->Form->control('instructor_id', ['options' => $instructors, 'empty' => true]);
            echo $this->Form->control('sex',['label'=>'Sexo','readonly']);
            echo $this->Form->control('idlegal',['label'=>'Identificación legal','readonly']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'),['class'=>'hiddenBtn', 'id'=>'mainSave']) ?>
    <?= $this->Form->end() ?>
    <?php echo $this->Form->button(__('Editar'),['id'=>'makeEditable'])?>

    <div class="related">
        <h4><?= __('Related Measurements') ?></h4>
        <?php if (!empty($subscriber->measurements)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Metabolicage') ?></th>
                <th scope="col"><?= __('Bmr') ?></th>
                <th scope="col"><?= __('Bonemass') ?></th>
                <th scope="col"><?= __('Height') ?></th>
                <th scope="col"><?= __('Weight') ?></th>
                <th scope="col"><?= __('Fat') ?></th>
                <th scope="col"><?= __('Neck') ?></th>
                <th scope="col"><?= __('Rightarm') ?></th>
                <th scope="col"><?= __('Leftarm') ?></th>
                <th scope="col"><?= __('Wrist') ?></th>
                <th scope="col"><?= __('Core') ?></th>
                <th scope="col"><?= __('Hip') ?></th>
                <th scope="col"><?= __('Thorax') ?></th>
                <th scope="col"><?= __('Righthigh') ?></th>
                <th scope="col"><?= __('Lefthigh') ?></th>
                <th scope="col"><?= __('Rightcalve') ?></th>
                <th scope="col"><?= __('Leftcalve') ?></th>
                <th scope="col"><?= __('Creationdate') ?></th>
                <th scope="col"><?= __('Subscriber Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($subscriber->measurements as $measurements): ?>
            <tr>
                <td><?= h($measurements->id) ?></td>
                <td><?= h($measurements->metabolicage) ?></td>
                <td><?= h($measurements->bmr) ?></td>
                <td><?= h($measurements->bonemass) ?></td>
                <td><?= h($measurements->height) ?></td>
                <td><?= h($measurements->weight) ?></td>
                <td><?= h($measurements->fat) ?></td>
                <td><?= h($measurements->neck) ?></td>
                <td><?= h($measurements->rightarm) ?></td>
                <td><?= h($measurements->leftarm) ?></td>
                <td><?= h($measurements->wrist) ?></td>
                <td><?= h($measurements->core) ?></td>
                <td><?= h($measurements->hip) ?></td>
                <td><?= h($measurements->thorax) ?></td>
                <td><?= h($measurements->righthigh) ?></td>
                <td><?= h($measurements->lefthigh) ?></td>
                <td><?= h($measurements->rightcalve) ?></td>
                <td><?= h($measurements->leftcalve) ?></td>
                <td><?= h($measurements->creationdate) ?></td>
                <td><?= h($measurements->subscriber_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Measurements', 'action' => 'view', $measurements->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Measurements', 'action' => 'edit', $measurements->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Measurements', 'action' => 'delete', $measurements->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measurements->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Routines') ?></h4>
        <?php if (!empty($subscriber->routines)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Subscriber Id') ?></th>
                <th scope="col"><?= __('Creationtimestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($subscriber->routines as $routines): ?>
            <tr>
                <td><?= h($routines->id) ?></td>
                <td><?= h($routines->type) ?></td>
                <td><?= h($routines->subscriber_id) ?></td>
                <td><?= h($routines->creationtimestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Routines', 'action' => 'view', $routines->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Routines', 'action' => 'edit', $routines->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Routines', 'action' => 'delete', $routines->id], ['confirm' => __('Are you sure you want to delete # {0}?', $routines->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

<?php echo $this->Html->script('General/editActions.js'); ?>
