<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Subscriber[]|\Cake\Collection\CollectionInterface $subscribers
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Subscriber'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Measurements'), ['controller' => 'Measurements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Measurement'), ['controller' => 'Measurements', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Routines'), ['controller' => 'Routines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Routine'), ['controller' => 'Routines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subscribers index large-9 medium-8 columns content">
    <h3><?= __('Subscriptores') ?></h3>
      <?php foreach ($subscribers as $subscriber): ?>
        <div class="card">
          <div class="photoContainer">
            <?= h($subscriber->photo) ?>
          </div>
          <div class="card-content">
            <?= h($subscriber->name) ?>
            <?= h($subscriber->lastnames) ?>
            <br>
            <?= h($subscriber->idlegal) ?>
            <br>
            <?= $this->Html->link(__('Ver / Editar'), ['action' => 'view', $subscriber->id]) ?>
          </div>
        </div>
      <?php endforeach; ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Principio')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
    </div>
</div>
