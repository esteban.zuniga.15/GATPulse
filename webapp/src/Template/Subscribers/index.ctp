<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Subscriber[]|\Cake\Collection\CollectionInterface $subscribers
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Subscriber'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Gymprofiles'), ['controller' => 'Gymprofiles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Gymprofile'), ['controller' => 'Gymprofiles', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Instructors'), ['controller' => 'Instructors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Instructor'), ['controller' => 'Instructors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Measurements'), ['controller' => 'Measurements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Measurement'), ['controller' => 'Measurements', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Routines'), ['controller' => 'Routines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Routine'), ['controller' => 'Routines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="subscribers index large-9 medium-8 columns content">
    <h3><?= __('Subscriptores') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name',['label'=>'Nombre']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('lastnames',['label'=>'Apellidos']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('birthday',['label'=>'Fecha de nacimiento']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('email',['label'=>'Correo']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone',['label'=>'Teléfono']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('joined',['label'=>'Fecha de ingreso']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('instructor_id',['label'=>'Instructor']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('sex',['label'=>'Sexo']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('idlegal',['label'=>'Identificación legal']) ?></th>
                <th scope="col" class="actions"><?= __('') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subscribers as $subscriber): ?>
            <tr>
                <td><?= h($subscriber->name) ?></td>
                <td><?= h($subscriber->lastnames) ?></td>
                <td><?= h(date("d/m/Y", strtotime($subscriber->birthday))) ?></td>
                <td><?= h($subscriber->email) ?></td>
                <td><?= h($subscriber->phone) ?></td>
                <td><?= h(date("d/m/Y", strtotime($subscriber->joined))) ?></td>
                <td><?= $subscriber->has('instructor') ? $this->Html->link($subscriber->instructor->name, ['controller' => 'Instructors', 'action' => 'view', $subscriber->instructor->id]) : '' ?></td>
                <td><?= h($subscriber->sex) ?></td>
                <td><?= h($subscriber->idlegal) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver / Editar'), ['action' => 'view', $subscriber->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $subscriber->id], ['confirm' => __('Are you sure you want to delete # {0}?', $subscriber->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Principio')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
    </div>
</div>
