/**
 * Created by donandrey on 10/5/17.
 */

class Host
{

    constructor(host_name = "http://localhost:8888/gatpulse-cake/webapp/")
    {
        this.host_name = host_name;
    }

     getHostName()
    {
        return this.host_name;
    }

     setHostName(name)
    {
        this.host_name = name;
    }
}

