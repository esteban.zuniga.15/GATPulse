demoUpload();

function demoUpload() {
  var $uploadCrop;

  function readFile(input) {
    if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
        $('#uploadCirclePhoto').addClass('ready');
              $uploadCrop.croppie('bind', {
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
          swal("Sorry - you're browser doesn't support the FileReader API");
      }
  }

  $uploadCrop = $('#uploadCirclePhoto').croppie({
    viewport: {
      width: 200,
      height: 200,
      type: 'circle'
    },
    enableExif: true
  });

  $('#upload').on('change', function () { readFile(this); });
  $('.uploadCroppie').on('click', function (ev) {
    $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function (resp) {
      $('#imagebase64').val(resp);
      $('#form').submit();
    });
  });
}
