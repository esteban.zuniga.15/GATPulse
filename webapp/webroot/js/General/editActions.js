if (document.getElementById("makeEditable")!=null) {
  document.getElementById("makeEditable").addEventListener('click',makeEditable);
}

function makeAllInputsChangeable() {
  $('input[readonly]').each(function (index, value) {
    $(this).attr('readonly',false);
  });
}


//Requires GeneralPurpose/hidden.css
function showBtn(btn) {
  btn.classList.remove('hiddenBtn');
  btn.classList.add('visibleBtn');
}
//Requires GeneralPurpose/hidden.css
function hideBtn(btn){
  btn.classList.remove('visibleBtn');
  btn.classList.add('hiddenBtn');
}

//Requires btn to have id "mainSave"
function makeEditable() {
  saveBtn = document.getElementById("mainSave");
  editBtn = document.getElementById("makeEditable")
  makeAllInputsChangeable();
  showBtn(saveBtn);
  hideBtn(editBtn);
}
