<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TrainingsFixture
 *
 */
class TrainingsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'instructor_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'gymprofile_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'capacity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'schedule_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'FKTrainingGym_idx' => ['type' => 'index', 'columns' => ['gymprofile_id'], 'length' => []],
            'FKInstructorTraining_idx' => ['type' => 'index', 'columns' => ['instructor_id'], 'length' => []],
            'FKTrainingSchedule_idx' => ['type' => 'index', 'columns' => ['schedule_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FKGymProfileTraining' => ['type' => 'foreign', 'columns' => ['gymprofile_id'], 'references' => ['gymprofiles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FKInstructorTraining' => ['type' => 'foreign', 'columns' => ['instructor_id'], 'references' => ['instructors', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FKScheduleTraining' => ['type' => 'foreign', 'columns' => ['schedule_id'], 'references' => ['schedules', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'instructor_id' => 1,
            'gymprofile_id' => 1,
            'capacity' => 1,
            'schedule_id' => 1
        ],
    ];
}
