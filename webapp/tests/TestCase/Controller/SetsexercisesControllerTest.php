<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SetsexercisesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SetsexercisesController Test Case
 */
class SetsexercisesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.setsexercises',
        'app.sets',
        'app.routines',
        'app.subscribers',
        'app.gymprofiles',
        'app.schedules',
        'app.instructors',
        'app.trainings',
        'app.users',
        'app.measurements',
        'app.exercises',
        'app.exercisetypes',
        'app.sets_exercises'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
