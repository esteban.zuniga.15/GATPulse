<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExercisetypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExercisetypesTable Test Case
 */
class ExercisetypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExercisetypesTable
     */
    public $Exercisetypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.exercisetypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Exercisetypes') ? [] : ['className' => ExercisetypesTable::class];
        $this->Exercisetypes = TableRegistry::get('Exercisetypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Exercisetypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
