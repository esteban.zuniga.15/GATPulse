<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SetsexercisesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SetsexercisesTable Test Case
 */
class SetsexercisesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SetsexercisesTable
     */
    public $Setsexercises;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.setsexercises',
        'app.sets',
        'app.routines',
        'app.subscribers',
        'app.gymprofiles',
        'app.schedules',
        'app.instructors',
        'app.trainings',
        'app.users',
        'app.measurements',
        'app.exercises',
        'app.exercisetypes',
        'app.sets_exercises'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Setsexercises') ? [] : ['className' => SetsexercisesTable::class];
        $this->Setsexercises = TableRegistry::get('Setsexercises', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Setsexercises);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
