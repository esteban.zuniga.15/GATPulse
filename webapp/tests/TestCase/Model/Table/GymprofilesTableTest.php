<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GymprofilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GymprofilesTable Test Case
 */
class GymprofilesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GymprofilesTable
     */
    public $Gymprofiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gymprofiles',
        'app.schedules',
        'app.subscribers',
        'app.trainings',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Gymprofiles') ? [] : ['className' => GymprofilesTable::class];
        $this->Gymprofiles = TableRegistry::get('Gymprofiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gymprofiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
